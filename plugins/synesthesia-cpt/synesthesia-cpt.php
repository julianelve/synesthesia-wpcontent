<?php

/**
 * Plugin Name: Synesthesia-CPT
 * Plugin URI: https://bitbucket.org/julianelve/wp-plugin-synesthesia-cpt
 * Description: Custom Post types for Synesthesia website
 * Version: 1.1.0
 * Author: Julian Elve
 * Author URI: https://bitbucket.org/julianelve
 * Text Domain: Optional. synesthesia
 * License: GPL2
 */


/*  Copyright 2015  Julian Elve(email : julian@elve.co.uk)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
* Changelog
* 00.00.0001: (16/02/15) Initial version
*
*
*/

define('SYNCPT_PLUGIN_DIR', trailingslashit(dirname(__FILE__)));
define('SYNCPT_PLUGIN_URL', str_replace(WP_PLUGIN_DIR, WP_PLUGIN_URL, SYNCPT_PLUGIN_DIR));
define('SYNCPT_INCLUDES_DIR', SYNCPT_PLUGIN_DIR . 'includes/');
$syncpt_plugin_name = 'Synesthesia Custom Post Types';

require_once(SYNCPT_INCLUDES_DIR . 'syn_pluginbase.php');

class synctp extends syn_pluginbase{

    function __construct() {
        global $syncpt_plugin_name;

        add_action('init', array($this, 'init'));
        add_action( 'wp_head', array($this,'add_cpt_feed_worknote' ));

    }

    public function init() {
        $this->register_post_types();
        add_filter( 'pre_get_posts', array($this, 'add_cpt_to_main_feed')) ;
    }

    public  function add_cpt_feed_worknote(){
        $this->add_cpt_feed('syn_worknote', 'Work Notes');
    }

    public  function add_cpt_to_main_feed($query){
        if ( $query->is_feed() )
            $query->set( 'post_type', array( 'post', 'syn_worknote' ) ); 

        return $query;
    }

   
    private function register_post_types()
    {
        $this->create_post_type_linklog();
        $this->create_post_type_worknote();
    }

    private function create_post_type_linklog()
    {
        $labels = array (
            'name'          => __( 'LinkLogs' ),
            'singular_name' => __( 'LinkLog' )
        );

        register_post_type( 'syn_linklog',
            array(
                'labels'        => $labels,
                'public'        => true,
                'has_archive'   => true,
                'menu_position' => 20,
                'rewrite'       => array ('slug' => 'linklog'),
                'supports'      => array( 'title', 'editor', 'author', 'comments', 'trackbacks','custom-fields' ),
                'taxonomies'    => array( 'category', 'post_tag')
            )
        );
    }

    private function create_post_type_worknote()
    {

        $labels = array (
            'name'          => __( 'Work Notes' ),
            'singular_name' => __( 'Work Note' )
        );

        register_post_type( 'syn_worknote',
            array(
                'labels'        => $labels,
                'public'        => true,
                'has_archive'   => true,
                'menu_position' => 20,
                'rewrite'       => array ('slug' => 'worknotes'),
                'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'trackbacks','custom-fields' ),
                'taxonomies'    => array( 'category', 'post_tag'),
                'show_in_rest'  => true,
                'rest_base'     => 'worknotes'
            )
        );
    }

    

    private function add_cpt_feed($cpt, $name){
        $post_types = array($cpt);
        foreach( $post_types as $post_type ) {
            $feed = get_post_type_archive_feed_link( $post_type );
            if ( $feed === '' || !is_string( $feed ) ) {
                $feed =  get_bloginfo( 'rss2_url' ) . "?post_type=$post_type";
            }
            $title = get_bloginfo('name') . ' » ' . $name . ' Feed';
            printf(__('<link rel="%1$s" type="%2$s" title="%4$s" href="%3$s" />'),"alternate","application/rss+xml",$feed, $title);
        }
    }


    /*add_action( 'init', 'create_post_type' );
        function create_post_type() {

        }*/

}

$synctp = new synctp();
?>
