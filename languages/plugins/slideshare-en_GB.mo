��          �      \      �  F   �          5  '   J  S   r  �   �  �   X  >   �       0   .     _     r  
   {     �  
   �     �  '   �  &   �       ;  '  F   c     �     �  '   �  S     �   X  �   �  >   o     �  0   �     �     	  
   	     	  
   1	     <	     W	  &   u	     �	                                  
                                     	                            A plugin for WordPress to easily display slideshare.net presentations. Explanation of default width Explanation of usage Give it a good rating on WordPress.org. If you <em>do</em> enter a value, it will always replace the width with that value. If you enter nothing in the setting above, you can change the width by hand by changing (or inserting) the w= value, that is bolded and red here: Just copy and paste the WordPress Embed code from %s and you're done. Or you can just put the presentation URL on a line of its own. Let other people know that it works with your WordPress setup. Like this plugin? Link to it so other folks can find out about it. Presentation width Settings SlideShare SlideShare Configuration Team Yoast Update SlideShare Settings Why not do any or all of the following: http://yoast.com/wordpress/slideshare/ https://yoast.com/ PO-Revision-Date: 2017-01-20 12:15:13+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-alpha
Language: en_GB
Project-Id-Version: Plugins - SlideShare for WordPress by Yoast - Stable (latest release)
 A plugin for WordPress to easily display slideshare.net presentations. Explanation of default width Explanation of usage Give it a good rating on WordPress.org. If you <em>do</em> enter a value, it will always replace the width with that value. If you enter nothing in the setting above, you can change the width by hand by changing (or inserting) the w= value, that is bolded and red here: Just copy and paste the WordPress Embed code from %s and you're done. Or you can just put the presentation URL on a line of its own. Let other people know that it works with your WordPress setup. Like this plugin? Link to it so other folks can find out about it. Presentation width Settings SlideShare SlideShare Configuration Team Yoast Update SlideShare Settings Consider doing the following: http://yoast.com/wordpress/slideshare/ https://yoast.com/ 